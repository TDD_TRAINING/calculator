package edu.tdd.calculator.displayer;

import org.junit.jupiter.api.Test;

import static org.mockito.Mockito.*;

class ResultDisplayerTest {
    private static final int ONE = 1;
    private final Displayer displayer = mock(Displayer.class);
    private final ResultDisplayer resultDisplayer = new ResultDisplayer(displayer);

    @Test
    void should_display_result_as_double() {
        resultDisplayer.displayResult(9.65d);

        verify(displayer, times(ONE)).display("Result: 9.65");
    }

    @Test
    void should_display_integer_without_trailing_decimals() {
        resultDisplayer.displayResult(5.00d);

        verify(displayer, times(ONE)).display("Result: 5");
    }

    @Test
    void should_display_error() {
        resultDisplayer.displayError("Bad format");

        verify(displayer, times(ONE)).display("Bad format");
    }
}
