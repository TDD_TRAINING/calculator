package edu.tdd.calculator.operands;

import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

class OperandsExtractorTest {
    private final OperandExtractor operandExtractor = new OperandExtractor();

    @Test
    void should_extract_a_single_operand() {
        assertThat(operandExtractor.extractOperands("2"))
                .isEqualTo(new Operands(0, 2));
    }

    @Test
    void should_extract_a_singed_singleton() {
        assertThat(operandExtractor.extractOperands("-2"))
                .isEqualTo(new Operands(0, 2));
        assertThat(operandExtractor.extractOperands("+2"))
                .isEqualTo(new Operands(0, 2));
    }

    @Test
    void should_extract_operands() {
        assertThat(operandExtractor.extractOperands("1+2"))
                .isEqualTo(new Operands(1, 2));
    }
}
