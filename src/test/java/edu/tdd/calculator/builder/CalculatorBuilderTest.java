package edu.tdd.calculator.builder;

import edu.tdd.calculator.calculator.Calculator;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

class CalculatorBuilderTest {
    @Test
    void should_instantiate_calculator() {
        assertThat(CalculatorBuilder.build()).isExactlyInstanceOf(Calculator.class);
    }
}
