package edu.tdd.calculator;

import edu.tdd.calculator.runner.CalculatorRunner;
import org.junit.jupiter.api.Test;

class CalculatorAcceptanceTest {
    @Test
    void should_calculate() {
        CalculatorRunner.run("8", "          +", "9   ");
        CalculatorRunner.run("6-2");
        CalculatorRunner.run("9", " *    ", "2   ", "7");
        CalculatorRunner.run("8", "          /", "9   ");
    }

    @Test
    void should_write_error() {
        CalculatorRunner.run("9/0");
        CalculatorRunner.run("8", "          +", "9   ", "*", "4");
        CalculatorRunner.run("8.3", "+", "5");
    }
}
