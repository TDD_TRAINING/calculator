package edu.tdd.calculator.parser;

import edu.tdd.calculator.exception.InvalidInputException;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;

class InputValidatorTest {
    private final InputValidator inputValidator = new InputValidator();

    @Test
    void should_accept_an_empty_input() {
        assertThat(inputValidator.validateInput("")).isTrue();
    }

    @Test
    void should_accept_a_single_operand() {
        assertThat(inputValidator.validateInput("15")).isTrue();
        assertThat(inputValidator.validateInput("+5")).isTrue();
        assertThat(inputValidator.validateInput("-8")).isTrue();
        assertThat(inputValidator.validateInput("8+")).isTrue();
        assertThat(inputValidator.validateInput("9-")).isTrue();
    }

    @Test
    void should_accept_two_operands_operations() {
        assertThat(inputValidator.validateInput("3+5")).isTrue();
        assertThat(inputValidator.validateInput("5-2")).isTrue();
        assertThat(inputValidator.validateInput("8*4")).isTrue();
        assertThat(inputValidator.validateInput("7/23")).isTrue();
    }

    @Test
    void should_reject_unsupported_operator() {
        Exception exception = assertThrows(InvalidInputException.class, () -> inputValidator.validateInput("3x5"));
        assertThat(exception.getMessage()).isEqualTo("Invalid Input: unsupported operation");
    }

    @Test
    void should_reject_operations_with_more_than_two_operands() {
        Exception exception = assertThrows(InvalidInputException.class, () -> inputValidator.validateInput("3+6*5"));
        assertThat(exception.getMessage()).isEqualTo("Invalid Input: more than two operands");
    }

    @Test
    void should_reject_operations_with_non_integer_operands() {
        Exception exception = assertThrows(InvalidInputException.class, () -> inputValidator.validateInput("3.6/x"));
        assertThat(exception.getMessage()).isEqualTo("Invalid Input: non integer operands");
    }

    @Test
    void should_reject_division_by_zero() {
        Exception exception = assertThrows(InvalidInputException.class, () -> inputValidator.validateInput("3/0"));
        assertThat(exception.getMessage()).isEqualTo("Invalid Input: cannot divide by zero");
    }
}
