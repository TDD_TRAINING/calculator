package edu.tdd.calculator.parser;

import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

class InputFormatterTest {
    private final InputFormatter inputFormatter = new InputFormatter();

    @Test
    void should_concatenate_and_trim_string_array() {
        assertThat(inputFormatter.formatInput("a          ", "           b", "c", "         d             "))
                .isEqualTo("abcd");
    }
}
