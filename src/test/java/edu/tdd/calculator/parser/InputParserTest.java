package edu.tdd.calculator.parser;

import edu.tdd.calculator.exception.InvalidInputException;
import edu.tdd.calculator.operation.OperationBuilder;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.*;

class InputParserTest {
    private static final int ONE = 1;
    private static final String[] input = {"3", " + ", "           8"};
    private static final String formattedInput = "3+8";

    private final InputFormatter inputFormatter = mock(InputFormatter.class);
    private final InputValidator inputValidator = mock(InputValidator.class);
    private final OperationBuilder operationBuilder = mock(OperationBuilder.class);

    private final InputParser inputParser = new InputParser(inputFormatter, inputValidator, operationBuilder);

    @Test
    void should_invoke_formatter_validator_and_operation_builder() {
        when(inputFormatter.formatInput(input)).thenReturn(formattedInput);

        inputParser.inputToOperation(input);

        verify(inputFormatter, times(ONE)).formatInput(input);
        verify(inputValidator, times(ONE)).validateInput(formattedInput);
        verify(operationBuilder, times(ONE)).buildFrom(formattedInput);
    }

    @Test
    void should_not_invoke_operation_builder_if_input_is_not_valid() {
        when(inputFormatter.formatInput(input)).thenReturn("");
        when(inputValidator.validateInput(anyString())).thenThrow(new InvalidInputException("Input is invalid"));

        Exception exception = assertThrows(InvalidInputException.class, () -> inputParser.inputToOperation(input));

        verify(inputFormatter, times(ONE)).formatInput(input);
        verify(inputValidator, times(ONE)).validateInput("");

        assertThat(exception.getMessage()).isEqualTo("Input is invalid");

        verifyNoInteractions(operationBuilder);
    }
}
