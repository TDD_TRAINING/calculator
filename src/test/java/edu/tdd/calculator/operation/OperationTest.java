package edu.tdd.calculator.operation;

import edu.tdd.calculator.operands.Operands;
import edu.tdd.calculator.operator.Operator;
import org.junit.jupiter.api.Test;

import static org.mockito.Mockito.*;

class OperationTest {
    private static final int ONE = 1;

    private final Operands operands = new Operands(1, 2);
    private final Operator operator = mock(Operator.class);

    private final Operation operation = new Operation(operands, operator);

    @Test
    void should_invoke_operator() {
        operation.calculate();

        verify(operator, times(ONE)).operate(operands);
    }
}
