package edu.tdd.calculator.operation;

import edu.tdd.calculator.operands.Operands;
import edu.tdd.calculator.operator.Division;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

class DivisionTest {
    @Test
    void should_divide_operands() {
        assertThat(new Division().operate(new Operands(5, 6))).isEqualTo(0.83333333333333333333d);
    }
}
