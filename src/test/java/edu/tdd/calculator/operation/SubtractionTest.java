package edu.tdd.calculator.operation;

import edu.tdd.calculator.operands.Operands;
import edu.tdd.calculator.operator.Subtraction;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

class SubtractionTest {
    @Test
    void should_subtract_operands() {
        assertThat(new Subtraction().operate(new Operands(8, 2))).isEqualTo(6d);
    }
}
