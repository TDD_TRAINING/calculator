package edu.tdd.calculator.operation;

import edu.tdd.calculator.operands.Operands;
import edu.tdd.calculator.operator.Multiplication;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

class MultiplicationTest {
    @Test
    void should_multiply_operands() {
        assertThat(new Multiplication().operate(new Operands(9, 5))).isEqualTo(45d);
    }
}
