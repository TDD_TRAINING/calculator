package edu.tdd.calculator.operation;

import edu.tdd.calculator.operands.OperandExtractor;
import edu.tdd.calculator.operands.Operands;
import edu.tdd.calculator.operator.Operator;
import edu.tdd.calculator.operator.OperatorFactory;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;

class OperationBuilderTest {
    private static final int ONE = 1;
    private static final String formattedInput = "formattedInput";

    private final OperandExtractor operandExtractor = mock(OperandExtractor.class);
    private final OperatorFactory operatorFactory = mock(OperatorFactory.class);
    private final OperationBuilder operationBuilder = new OperationBuilder(operandExtractor, operatorFactory);

    @Test
    void should_invoke_extractor_and_factory() {
        Operands operands = new Operands(1, 2);
        Operator operator = mock(Operator.class);

        when(operandExtractor.extractOperands(formattedInput)).thenReturn(operands);
        when(operatorFactory.getOperator(formattedInput)).thenReturn(operator);

        Operation operation = operationBuilder.buildFrom(formattedInput);

        verify(operandExtractor, times(ONE)).extractOperands(formattedInput);
        verify(operatorFactory, times(ONE)).getOperator(formattedInput);

        assertThat(operation).isEqualTo(new Operation(operands, operator));
    }
}
