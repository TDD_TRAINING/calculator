package edu.tdd.calculator.operation;

import edu.tdd.calculator.operands.Operands;
import edu.tdd.calculator.operator.Addition;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

class AdditionTest {
    @Test
    void should_add_operands() {
        assertThat(new Addition().operate(new Operands(5, 2))).isEqualTo(7d);
    }
}
