package edu.tdd.calculator.operator;

import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

class OperatorFactoryTest {
    private final OperatorFactory operatorFactory = new OperatorFactory();

    @Test
    void should_return_identity() {
        assertThat(operatorFactory.getOperator("A"))
                .isExactlyInstanceOf(Identity.class);
    }

    @Test
    void should_return_an_addition() {
        assertThat(operatorFactory.getOperator("A+B"))
                .isExactlyInstanceOf(Addition.class);
    }

    @Test
    void should_return_an_subtraction() {
        assertThat(operatorFactory.getOperator("A-B"))
                .isExactlyInstanceOf(Subtraction.class);
    }

    @Test
    void should_return_a_multiplication() {
        assertThat(operatorFactory.getOperator("3*4"))
                .isExactlyInstanceOf(Multiplication.class);
    }

    @Test
    void should_return_a_division() {
        assertThat(operatorFactory.getOperator("3/4"))
                .isExactlyInstanceOf(Division.class);
    }
}
