package edu.tdd.calculator.calculator;

import edu.tdd.calculator.exception.InvalidInputException;
import edu.tdd.calculator.operation.Operation;
import edu.tdd.calculator.parser.InputParser;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.*;

class CalculatorTest {
    private static final int ONE = 1;
    private static final String[] input = {"3", "+", "5"};

    private final InputParser inputParser = mock(InputParser.class);
    private final Calculator calculator = new Calculator(inputParser);

    @Test
    void should_invoke_parser_and_displayer() {
        Operation operation = mock(Operation.class);
        double result = 42;
        when(inputParser.inputToOperation(input)).thenReturn(operation);
        when(operation.calculate()).thenReturn(result);

        calculator.calculate(input);

        verify(inputParser, times(ONE)).inputToOperation(input);
        verify(operation, times(ONE)).calculate();
    }

    @Test
    void should_display_error_message_if_exception_has_been_raised() {
        Operation operation = mock(Operation.class);
        when(inputParser.inputToOperation(input)).thenThrow(new InvalidInputException("Input is invalid"));

        assertThrows(InvalidInputException.class, () -> calculator.calculate(input));

        verify(inputParser, times(ONE)).inputToOperation(input);
        verifyNoInteractions(operation);
    }
}
