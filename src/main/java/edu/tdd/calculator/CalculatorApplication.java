package edu.tdd.calculator;

import edu.tdd.calculator.runner.CalculatorRunner;

public class CalculatorApplication {
    public static void main(String... args) {
        CalculatorRunner.run(args);
    }
}
