package edu.tdd.calculator.operator;

import edu.tdd.calculator.operands.Operands;

public interface Operator {
    double operate(Operands operands);
}
