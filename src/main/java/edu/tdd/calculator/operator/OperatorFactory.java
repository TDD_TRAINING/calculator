package edu.tdd.calculator.operator;

import java.util.Map;
import java.util.function.Supplier;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static edu.tdd.calculator.operator.OperatorsUtils.ALLOWED_OPERATORS;

public class OperatorFactory {
    Map<String, Supplier<Operator>> operators = Map.of(
            "Identity", Identity::new,
            "+", Addition::new,
            "-", Subtraction::new,
            "*", Multiplication::new,
            "/", Division::new
    );

    public Operator getOperator(String validatedInput) {
        String operator = extractOperator(validatedInput);
        return operators.get(operator).get();
    }

    private String extractOperator(String validatedInput) {
        Matcher matcher = Pattern.compile(ALLOWED_OPERATORS).matcher(validatedInput);
        if (matcher.find())
            return matcher.group();

        return "Identity";
    }
}
