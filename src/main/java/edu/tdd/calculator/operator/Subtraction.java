package edu.tdd.calculator.operator;

import edu.tdd.calculator.operands.Operands;

import static edu.tdd.calculator.operator.OperatorsUtils.dooble;

public class Subtraction implements Operator {
    @Override
    public double operate(Operands operands) {
        return dooble(operands.getLeft()) - dooble(operands.getRight());
    }
}
