package edu.tdd.calculator.operands;

import static edu.tdd.calculator.operator.OperatorsUtils.ALLOWED_OPERATORS;

public class OperandExtractor {
    public Operands extractOperands(String validatedInput) {
        String[] operands = validatedInput.split(ALLOWED_OPERATORS);
        return operands.length == 1 ? singleton(operands) : pair(operands);
    }

    private Operands singleton(String[] operands) {
        return new Operands(0, safeInt(operands[0]));
    }

    private Operands pair(String[] operands) {
        return new Operands(safeInt(operands[0]), safeInt(operands[1]));
    }

    private int safeInt(String operand) {
        if (operand.trim().isEmpty())
            return 0;
        return Integer.parseInt(operand);
    }
}
