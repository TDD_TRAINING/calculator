package edu.tdd.calculator.operands;

import lombok.Value;

@Value
public class Operands {
    int left;
    int right;
}
