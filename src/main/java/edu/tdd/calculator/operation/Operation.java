package edu.tdd.calculator.operation;

import edu.tdd.calculator.operands.Operands;
import edu.tdd.calculator.operator.Operator;
import lombok.Value;

@Value
public class Operation {
    Operands operands;
    Operator operator;

    public double calculate() {
        return operator.operate(operands);
    }
}
