package edu.tdd.calculator.operation;

import edu.tdd.calculator.operands.OperandExtractor;
import edu.tdd.calculator.operands.Operands;
import edu.tdd.calculator.operator.Operator;
import edu.tdd.calculator.operator.OperatorFactory;
import lombok.AllArgsConstructor;

@AllArgsConstructor
public class OperationBuilder {
    private final OperandExtractor operandExtractor;
    private final OperatorFactory operatorFactory;

    public Operation buildFrom(String formattedInput) {
        Operands operands = operandExtractor.extractOperands(formattedInput);
        Operator operator = operatorFactory.getOperator(formattedInput);
        return new Operation(operands, operator);
    }
}
