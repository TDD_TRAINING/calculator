package edu.tdd.calculator.builder;

import edu.tdd.calculator.calculator.Calculator;
import edu.tdd.calculator.operands.OperandExtractor;
import edu.tdd.calculator.operation.OperationBuilder;
import edu.tdd.calculator.operator.OperatorFactory;
import edu.tdd.calculator.parser.InputFormatter;
import edu.tdd.calculator.parser.InputParser;
import edu.tdd.calculator.parser.InputValidator;

public class CalculatorBuilder {
    public static Calculator build() {
        InputFormatter inputFormatter = new InputFormatter();
        InputValidator inputValidator = new InputValidator();

        OperandExtractor operandExtractor = new OperandExtractor();
        OperatorFactory operatorFactory = new OperatorFactory();
        OperationBuilder operationBuilder = new OperationBuilder(operandExtractor, operatorFactory);

        InputParser inputParser = new InputParser(inputFormatter, inputValidator, operationBuilder);

        return new Calculator(inputParser);
    }
}
