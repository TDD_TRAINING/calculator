package edu.tdd.calculator.calculator;

import edu.tdd.calculator.operation.Operation;
import edu.tdd.calculator.parser.InputParser;
import lombok.AllArgsConstructor;

@AllArgsConstructor
public class Calculator {
    private final InputParser inputParser;

    public double calculate(String... input) {
        Operation operation = inputParser.inputToOperation(input);
        return operation.calculate();
    }
}
