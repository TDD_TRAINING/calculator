package edu.tdd.calculator.displayer;

import lombok.AllArgsConstructor;

@AllArgsConstructor
public class ResultDisplayer {
    private final Displayer displayer;

    public void displayResult(double result) {
        displayer.display("Result: " + displayedValue(result));
    }

    public void displayError(String errorMessage) {
        displayer.display(errorMessage);
    }

    private String displayedValue(double result) {
        return isInt(result) ? Integer.toString(intValue(result)) : Double.toString(result);
    }

    private boolean isInt(double result) {
        return Math.floor(result) == Math.ceil(result);
    }

    private int intValue(double result) {
        return (int) result;
    }
}
