package edu.tdd.calculator.parser;

import edu.tdd.calculator.exception.InvalidInputException;

import static edu.tdd.calculator.operator.OperatorsUtils.ALLOWED_OPERATORS;

public class InputValidator {
    public boolean validateInput(String formattedInput) {
        if (isEmptyOrBlank(formattedInput) || isSingleInteger(formattedInput)) {
            return true;
        }

        checkForInvalidInput(formattedInput);

        return isValidInput(formattedInput);
    }

    private boolean isEmptyOrBlank(String input) {
        return input.trim().isEmpty();
    }

    private boolean isSingleInteger(String input) {
        String[] operands = input.split(ALLOWED_OPERATORS);
        return operands.length == 1 && isInt(operands[0]);
    }

    private boolean isValidInput(String input) {
        return input.split(ALLOWED_OPERATORS).length == 2;
    }

    private void checkForInvalidInput(String input) {
        if (isOperatorUnsupported(input))
            throw new InvalidInputException("Invalid Input: unsupported operation");

        if (isNumberOfOperandsGreaterThanTwo(input))
            throw new InvalidInputException("Invalid Input: more than two operands");

        if (!areOperandsInteger(input))
            throw new InvalidInputException("Invalid Input: non integer operands");

        if (isDividingByZero(input))
            throw new InvalidInputException("Invalid Input: cannot divide by zero");
    }

    private boolean isOperatorUnsupported(String input) {
        return input.split(ALLOWED_OPERATORS).length == 1 && !isInt(input);
    }

    private boolean isNumberOfOperandsGreaterThanTwo(String input) {
        return input.split(ALLOWED_OPERATORS).length > 2;
    }

    private boolean areOperandsInteger(String input) {
        String[] operands = input.split(ALLOWED_OPERATORS);
        return isInt(operands[0]) && isInt(operands[1]);
    }

    private boolean isDividingByZero(String input) {
        String[] operands = input.split(ALLOWED_OPERATORS);
        return input.contains("/") && "0".equals(operands[1]);
    }

    private boolean isInt(String operand) {
        if (operand.isEmpty())
            return true;

        try {
            Integer.parseInt(operand);
        } catch (NumberFormatException exception) {
            return false;
        }
        return true;
    }
}
