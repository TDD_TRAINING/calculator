package edu.tdd.calculator.parser;

import edu.tdd.calculator.operation.Operation;
import edu.tdd.calculator.operation.OperationBuilder;
import lombok.AllArgsConstructor;

@AllArgsConstructor
public class InputParser {
    private final InputFormatter inputFormatter;
    private final InputValidator inputValidator;
    private final OperationBuilder operationBuilder;

    public Operation inputToOperation(String... input) {
        String formattedInput = inputFormatter.formatInput(input);
        inputValidator.validateInput(formattedInput);

        return operationBuilder.buildFrom(formattedInput);
    }
}
