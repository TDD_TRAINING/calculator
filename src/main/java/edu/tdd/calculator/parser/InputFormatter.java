package edu.tdd.calculator.parser;

import java.util.Arrays;

import static java.util.stream.Collectors.joining;

public class InputFormatter {
    public String formatInput(String... input) {
        return Arrays.stream(input)
                .map(String::trim)
                .collect(joining());
    }
}
