package edu.tdd.calculator.runner;

import edu.tdd.calculator.builder.CalculatorBuilder;
import edu.tdd.calculator.calculator.Calculator;
import edu.tdd.calculator.displayer.Displayer;
import edu.tdd.calculator.displayer.ResultDisplayer;
import edu.tdd.calculator.exception.InvalidInputException;

public class CalculatorRunner {
    private CalculatorRunner() {
    }

    public static void run(String... args) {
        Calculator calculator = CalculatorBuilder.build();
        ResultDisplayer resultDisplayer = new ResultDisplayer(new Displayer());

        try {
            double result = calculator.calculate(args);
            resultDisplayer.displayResult(result);
        } catch (InvalidInputException exception) {
            resultDisplayer.displayError(exception.getMessage());
        }
    }
}
