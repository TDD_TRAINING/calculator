# Smart Calculator

## Background

This kata is inspired by the "simple calculator kata".

We propose to implement a calculator that supports several operations, rather than just focusing on the format of the input line. This will lead us to learn more about some interesting patterns and coding and testing techniques.

The outstanding coding techniques we want to share is **refactoring before introducing a new functionality** and **outside-in method**. Both techniques are useful in real projects.

## Just before jumping in

We suggest that you take requirements one at the time, before moving to the next one. This step by step approach reminds of _User Stories_ we encounter in agile projects. At each step there should be no over engineering. However testing coverage should be optimal.

_**Watch-out!**_

Requirements are listed in a way that could be logical for a business analyst, but maybe not in the order they should be implemented. As developers, you will have to decide in which order the features should be implemented.

Throughout the requirements, we have outlined specific words that are part of the _**Domain Language**_. We suggest that these words (with strong underlying context) should appear in your code.

## Requirements

The `SmartCalculator` should

1. be able to _perform_ `Addition`, `Subtraction`, `Multiplication` and `Division` on integers
2. return a singleton `Operand` as it is
3. process String _inputs_, as in "1+2", "4-1", "2*3", "5/8" or "8"
4. return 0 when input is _empty_
5. return 0 when input is _blank_
6. reject `Operations` on _non-integer_ `Operands`
7. reject `Operations` with _more than two_ `Operands`
8. reject unsupported `Operations`
9. reject division by 0
10. reject non integer inputs
11. display integers as integers and floats as floats
